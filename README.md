# 青锋开源架构-springboot2.6.x+vue3-antdesign-vite

#### 介绍
青锋-springboot2.6.x+vue3-antdesign-vite开源架构，实现了系统管理模块、权限控制模块（菜单权限、功能按钮权限、数据权限）、代码生成器（单表、树表）、quartz动态定时器等功能。 声明：代码开源之处难免不足之处，还望大家多多指教。 目前项目已全部开源，需要的直接下载即可，数据脚本在项目目录下面。愿意交流的朋友可以加下作者的微信：QF_qingfeng1024

#### 软件架构
软件架构说明

- springboot2.6.x
- mybatis-plus
- mysql
- quartz
- vue3
- antdesign
- vite

#### 青锋官网

- [http://www.qingfeng.plus/](http://www.qingfeng.plus/)
- 预览地址：[前后端分离（单体/微服务）vue-低代码平台-自定义表单+activitiOA工作流](http://www.qingfeng.plus:8081/)
- 预览地址：[springboot+layui实现的低代码平台-自定义表单+拖拽表单+拖拽报表+activitiOA工作流](http://oa.qingfeng.pub:8181/qingfeng/main)


#### 其他产品

1. [springboot+Thymeleaf版](https://gitee.com/msxy/qingfengThymeleaf)
2. [springboot+Jsp版](https://gitee.com/msxy/qingfeng)
3. [springboot+VUE2版](https://gitee.com/msxy/qingfeng)
4. [springcloud+vue2/react-pro4版](https://gitee.com/msxy/qingfeng-cloud)
5. [springboot青锋家谱](https://gitee.com/msxy/qingfeng-gen)


#### 运行教程



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

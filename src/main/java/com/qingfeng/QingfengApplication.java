package com.qingfeng;

import com.qingfeng.base.annotation.EnableMyProtect;
import com.qingfeng.base.annotation.EnableMyRedis;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.qingfeng.*.mapper")
@EnableMyRedis
@EnableMyProtect
@ServletComponentScan("com.qingfeng.framework.servlet")
public class QingfengApplication {

    public static void main(String[] args) {
        SpringApplication.run(QingfengApplication.class, args);
    }

}
